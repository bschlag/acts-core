stages:
  - build
  - test
  - static_analysis
  - deploy

before_script:
  # CI executor uses fail on error by default
  # setup scripts do not like that
  - set +e && source CI/setup_lcg94.sh; set -e

# pre-build checks

format:
  stage: build
  image: gitlab-registry.cern.ch/acts/machines/check:latest
  before_script: []
  dependencies: []
  script:
    - CI/check_format .
  artifacts:
    paths:
      - changed
    when: on_failure

license:
  stage: build
  image: python:alpine3.6
  before_script: []
  dependencies: []
  script:
    - apk add --no-cache git
    - CI/check_license.py . --check-years

  
include_guards:
  stage: build
  image: python:alpine3.6
  before_script: []
  dependencies: []
  script:
    - CI/check_include_guards.py . --fail-global

clang_tidy:
  stage: static_analysis
  image: gitlab-registry.cern.ch/acts/machines/static_analysis/cc7-clang-tidy
  tags:
    - cvmfs
  before_script: []
  dependencies: []
  script:
    - mkdir report
    - mkdir build && cd build
    # contain LCG to only where needed
    - /bin/sh -c "set +e && source ../CI/setup_lcg94.sh; set -e && cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DACTS_BUILD_EXAMPLES=on -DACTS_BUILD_TESTS=on -DACTS_BUILD_INTEGRATION_TESTS=on -DACTS_BUILD_DD4HEP_PLUGIN=on -DACTS_BUILD_MATERIAL_PLUGIN=on -DACTS_BUILD_TGEO_PLUGIN=on -DACTS_BUILD_JSON_PLUGIN=on -DACTS_BUILD_DIGITIZATION_PLUGIN=on -DACTS_BUILD_IDENTIFICATION_PLUGIN=on"
    - cd ..
    - run-clang-tidy.py -p build -header-filter=.* -export-fixes report/clang-tidy-fixes.yml -j$(nproc) 1> report/clang-tidy.log || true
    - virtualenv civenv
    - source civenv/bin/activate
    - pip install -r CI/requirements.txt
    - CI/make_report.py clang-tidy report/clang-tidy.log report/clang-tidy.json -e "$PWD/Tests/*" -e "$PWD/Legacy/*" -e "$PWD/Plugins/Json/include/Acts/Plugins/Json/lib/*" --filter "$PWD/*"
    - 'codereport report/clang-tidy.json report --title="clang-tidy - commit: ${CI_COMMIT_SHA:0:8}" --prefix $PWD'
    - CI/static_analysis_results.py --limitfile .static_analysis_limits.yml --itemfile report/clang-tidy.json -v
  artifacts:
    paths:
      - report
    when: always

    


# job templates w/ default settings
#
# the build directory is always `build`. To select a specific build version
# you always have to define the correct `dependencies` settings to only
# load the artifacts from one build.

.build: &template_build
  stage: build
  image: gitlab-registry.cern.ch/acts/machines/slc6:latest
  tags:
    - cvmfs
  variables:
    BUILD_TYPE: Release
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DACTS_BUILD_EXAMPLES=on -DACTS_BUILD_TESTS=on -DACTS_BUILD_INTEGRATION_TESTS=on -DACTS_BUILD_DD4HEP_PLUGIN=on -DACTS_BUILD_MATERIAL_PLUGIN=on -DACTS_BUILD_TGEO_PLUGIN=on -DACTS_BUILD_DIGITIZATION_PLUGIN=on -DACTS_BUILD_IDENTIFICATION_PLUGIN=on -DACTS_BUILD_JSON_PLUGIN=on -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - cmake --build . -- -j${ACTS_NCPUS}
    - find . -name "*.o" -delete
  artifacts:
    paths:
      - build
    expire_in: 6 hours

.unit_tests: &template_unit_tests
  stage: test
  image: gitlab-registry.cern.ch/acts/machines/slc6:latest
  tags:
    - cvmfs
  variables:
      CTEST_OUTPUT_ON_FAILURE: 1
  script:
    - cmake --build build -- test
  artifacts:
    paths:
      - build
    expire_in: 6 hours

.integration_tests: &template_integration_tests
  stage: test
  image: gitlab-registry.cern.ch/acts/machines/slc6:latest
  tags:
    - cvmfs
  variables:
      CTEST_OUTPUT_ON_FAILURE: 1
  script:
    - cmake --build build -- integration_tests


# BUILD WITH LEGACY DISABLED
# ONLY THIS ONE JOB
build_slc6_lcg94_no_legacy:
  <<: *template_build
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DACTS_BUILD_EXAMPLES=on -DACTS_BUILD_TESTS=on -DACTS_BUILD_INTEGRATION_TESTS=on -DACTS_BUILD_DD4HEP_PLUGIN=on -DACTS_BUILD_MATERIAL_PLUGIN=on -DACTS_BUILD_TGEO_PLUGIN=on -DACTS_BUILD_LEGACY=off -DACTS_BUILD_DIGITIZATION_PLUGIN=on -DACTS_BUILD_IDENTIFICATION_PLUGIN=on -DACTS_BUILD_JSON_PLUGIN=on -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - cmake --build . -- -j${ACTS_NCPUS}
    - find . -name "*.o" -delete

# SLC6, LCG93

build_slc6_lcg93:
  <<: *template_build
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

unit_tests_slc6_lcg93:
  <<: *template_unit_tests
  dependencies:
    - build_slc6_lcg93
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

integration_tests_slc6_lcg93:
  <<: *template_integration_tests
  dependencies:
    - build_slc6_lcg93
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

# SLC6, LCG94 (default build)

build_slc6_lcg94:
  <<: *template_build
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
  variables:
    BUILD_TYPE: Debug

# this build tests whether we can do an incremental build
# from the last tag to this commit.
build_slc6_lcg94_incremental:
  <<: *template_build
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
  script:
    - source CI/incremental_build_setup.sh || true
    - export current_head=$(git rev-parse HEAD)
    - git checkout $prev_tag
    - mkdir build
    - cd build
    - cmake -GNinja -DACTS_BUILD_EXAMPLES=on -DACTS_BUILD_TESTS=on -DACTS_BUILD_INTEGRATION_TESTS=on -DACTS_BUILD_DD4HEP_PLUGIN=on -DACTS_BUILD_MATERIAL_PLUGIN=on -DACTS_BUILD_TGEO_PLUGIN=on -DACTS_BUILD_DIGITIZATION_PLUGIN=on -DACTS_BUILD_IDENTIFICATION_PLUGIN=on -DACTS_BUILD_JSON_PLUGIN=on -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - 'echo "Build previous tag: $prev_tag"'
    - cmake --build . -- -j${ACTS_NCPUS}
    - git checkout $current_head
    - 'echo "Build current head: $current_head"'
    - cmake --build . -- -j${ACTS_NCPUS}
    - find . -name "*.o" -delete
  allow_failure: true

unit_tests_slc6_lcg94:
  <<: *template_unit_tests
  dependencies:
    - build_slc6_lcg94
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
  # replace script to also calculate code coverage
  script:
    - cmake --build build -- test
    - cd build
    - ../CI/test_coverage
  coverage: '/^TOTAL.*\s+(\d+\%)$/'
  artifacts:
    paths:
      - build/coverage

integration_tests_slc6_lcg94:
  <<: *template_integration_tests
  dependencies:
    - build_slc6_lcg94
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e

# SLC, LCG94, LLVM40

build_slc6_lcg94_clang:
  <<: *template_build
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
    - source CI/setup_clang.sh

unit_tests_slc6_lcg94_clang:
  <<: *template_unit_tests
  dependencies:
    - build_slc6_lcg94_clang
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
    - source CI/setup_clang.sh

integration_tests_slc6_lcg94_clang:
  <<: *template_integration_tests
  dependencies:
    - build_slc6_lcg94_clang
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e
    - source CI/setup_clang.sh

# CC7, LCG93

build_cc7_lcg93:
  <<: *template_build
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DACTS_BUILD_EXAMPLES=on -DACTS_BUILD_TESTS=on -DACTS_BUILD_INTEGRATION_TESTS=on -DACTS_BUILD_DD4HEP_PLUGIN=off -DACTS_BUILD_MATERIAL_PLUGIN=on -DACTS_BUILD_TGEO_PLUGIN=off -DACTS_BUILD_DIGITIZATION_PLUGIN=on -DACTS_BUILD_IDENTIFICATION_PLUGIN=on -DACTS_BUILD_JSON_PLUGIN=on -DCMAKE_CXX_FLAGS="-Werror" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ..
    - cmake --build . -- -j${ACTS_NCPUS}
    - find . -name "*.o" -delete
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

unit_tests_cc7_lcg93:
  <<: *template_unit_tests
  dependencies:
    - build_cc7_lcg93
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

integration_tests_cc7_lcg93:
  <<: *template_integration_tests
  dependencies:
    - build_cc7_lcg93
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  before_script:
    - set +e && source CI/setup_lcg93.sh; set -e

# CC7, LCG94

build_cc7_lcg94:
  <<: *template_build
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e

unit_tests_cc7_lcg94:
  <<: *template_unit_tests
  dependencies:
    - build_cc7_lcg94
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e

integration_tests_cc7_lcg94:
  <<: *template_integration_tests
  dependencies:
    - build_cc7_lcg94
  image: gitlab-registry.cern.ch/acts/machines/cc7:latest
  before_script:
    - set +e && source CI/setup_lcg94.sh; set -e

doc:
  stage: build
  tags:
    - cvmfs
  script:
    - mkdir build
    - cd build
    - cmake -GNinja -DACTS_BUILD_DOC=on -DACTS_BUILD_TESTS=off ..
    - cmake --build . -- doc
  artifacts:
    paths:
      - build/doc/html

doc_deploy:
  stage: deploy
  dependencies:
    - doc
  script:
    - unset PYTHONHOME
    - echo "$ATSJENKINS_PASSWORD" | kinit atsjenkins@CERN.CH 2>&1 >/dev/null
    - CI/deploy_tag . build/doc/html "acts-developers@cern.ch acts-users@cern.ch"
  only:
    - tags

website_deploy:
  stage: deploy
  dependencies: []
  script:
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.cern.ch/api/v4/projects/36428/trigger/pipeline
  only:
    - master
    - tags

coverage_deploy:
  stage: deploy
  image: alpine:3.8
  dependencies:
    - unit_tests_slc6_lcg94
  before_script:
    - apk --no-cache add python3 py3-pynacl py3-pip py3-cryptography py3-bcrypt
    - pip3 install -r CI/requirements.txt
  script:
    - CI/publish_coverage.py --coverage-source=build/coverage --project-id=$CI_PROJECT_ID
  only:
    - master


sync_releases:
  stage: deploy
  image: alpine:3.8
  before_script:
    - apk --no-cache add krb5-dev python3 py3-cffi alpine-sdk python3-dev libffi-dev libxml2-dev libxslt-dev py3-lxml openssl-dev krb5 py3-pynacl
    - pip3 install --upgrade pip
    - pip3 install python-cern-sso-krb
    - pip3 install -r CI/requirements.txt
  script:
    - unset PYTHONHOME
    - echo "$ATSJENKINS_PASSWORD" | kinit atsjenkins@CERN.CH 2>&1 >/dev/null
    - CI/sync_releases.py
  after_script:
    - kdestroy
  only:
    - tags


